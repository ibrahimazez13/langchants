export const API_DOCS_V1 = 'api-docs/v1';

export const MEDIAS_API_PATH = 'medias';
export const REQUEST_TIMEOUT = 'REQUEST_TIMEOUT';
export const DATE_FORMAT = 'YYYY-MM-DD';
export const TIME_FORMAT = 'HH:mm';
