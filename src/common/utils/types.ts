import { IsAny } from 'type-fest';

export type Pagination = {
  limit: number;
  skip: number;
};
export const LanguageCode = {
  'en-US': 'en-US',
  'ar-SY': 'ar-SY',
} as const;

export type LanguageCode = (typeof LanguageCode)[keyof typeof LanguageCode];

export type PathImpl<T, Key extends keyof T> = Key extends string
  ? IsAny<T[Key]> extends true
    ? never
    : // eslint-disable-next-line
    T[Key] extends Record<string, any>
    ? // eslint-disable-next-line
      | `${Key}.${PathImpl<T[Key], Exclude<keyof T[Key], keyof any[]>> &
            string}`
        // eslint-disable-next-line
        | `${Key}.${Exclude<keyof T[Key], keyof any[]> & string}`
    : never
  : never;
export type PathImpl2<T> = PathImpl<T, keyof T> | keyof T;
