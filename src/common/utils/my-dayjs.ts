import dayjs, { Dayjs } from 'dayjs';
import advancedFormat from 'dayjs/plugin/advancedFormat';
import calendar from 'dayjs/plugin/calendar';
import duration from 'dayjs/plugin/duration';
import isSameOrAfter from 'dayjs/plugin/isSameOrAfter';
import isSameOrBefore from 'dayjs/plugin/isSameOrBefore';
import isToday from 'dayjs/plugin/isToday';
import timezone from 'dayjs/plugin/timezone';
import utc from 'dayjs/plugin/utc';
import weekday from 'dayjs/plugin/weekday';

import { DATE_FORMAT, TIME_FORMAT } from './constants';

dayjs.extend(advancedFormat);
dayjs.extend(utc);
dayjs.extend(weekday);
dayjs.extend(calendar);
dayjs.extend(duration);
dayjs.extend(timezone);
dayjs.extend(isSameOrBefore);
dayjs.extend(isSameOrAfter);
dayjs.extend(isToday);

type DateType = string | number | Date | Dayjs;
export const myDayjs = dayjs;

export const isDateInPresent = (date: DateType) =>
  myDayjs.utc(date).isAfter(now());

export const isDateInPast = (date: DateType) =>
  myDayjs.utc(date).isBefore(now());

export const isDateToday = (date: DateType) => myDayjs.utc(date).isToday();

export const toUtc = (date: DateType) => myDayjs.utc(date).toISOString();
export const toIso8601 = (date: DateType) => myDayjs(date).toISOString();
export const toIso8601WithEmptyTime = (date: DateType) =>
  toUtc(getDateWithoutTime(date));

export const now = () => new Date(toUtc(new Date(Date.now())));

export function getDateWithoutTime(date: DateType) {
  return dayjs.utc(date).format(DATE_FORMAT);
}

export function getDay(dates: MaybeArray<DateType>) {
  if (Array.isArray(dates))
    return dates.map((date) => parseInt(dayjs(date).utc().format('DD')));

  return parseInt(dayjs(dates).utc().format('DD'));
}

export function getMonth(dates: MaybeArray<DateType>) {
  if (Array.isArray(dates))
    return dates.map((date) => parseInt(dayjs(date).utc().format('MM')));

  return parseInt(dayjs(dates).utc().format('MM'));
}

export function getYear(dates: MaybeArray<DateType>) {
  if (Array.isArray(dates))
    return dates.map((date) => parseInt(dayjs(date).utc().format('YYYY')));

  return parseInt(dayjs(dates).utc().format('YYYY'));
}

export function getDifference(start: DateType, end: DateType) {
  return dayjs(start).hour(dayjs(start).diff(end, 'hours')).format(TIME_FORMAT);
}

export function getDateWithTime(dates: MaybeArray<DateType>) {
  if (Array.isArray(dates))
    return dates.map((date) => dayjs.utc(date).toISOString());

  return dayjs.utc(dates).toISOString();
}

export function isSameDate(start: DateType, end: DateType) {
  dayjs(getDateWithoutTime(start)).isSame(getDateWithoutTime(end));
}
