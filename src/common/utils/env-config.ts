import { config } from 'dotenv';

import { Logger } from '@nestjs/common';

config();

function getEnvVariable<U extends Stringish>(key: string, defaultValue?: U): U {
  const envKey = process.env[key];

  if (envKey !== undefined) {
    return String(envKey) as U;
  }

  if (defaultValue !== undefined) {
    Logger.warn(
      `can't find environment variable ${key}, will fallback on default value ${defaultValue}`,
    );
    return String(defaultValue) as U;
  }

  throw new Error(`can't find environment variable ${key}, and it is required`);
}

export const NodeEnv = {
  development: 'development',
  production: 'production',
  staging: 'staging',
  test: 'test',
} as const;

export type NodeEnv = (typeof NodeEnv)[keyof typeof NodeEnv];

export const envConfig: EnvConfig = {
  DATABASE_URL: getEnvVariable(
    'DATABASE_URL',
    'postgresql://postgres:password@127.0.0.1:5432/postgres',
  ),
  PORT: parseInt(getEnvVariable<`${number}`>('PORT', '3000')),
  REQUEST_TIMEOUT: parseInt(
    getEnvVariable<`${number}`>('REQUEST_TIMEOUT', '600000'),
  ),
  MODE: getEnvVariable('MODE', 'OTHER'),
  NODE_ENV: getEnvVariable<NodeEnv>('NODE_ENV', 'development'),
  IS_PRODUCTION_ENV: getEnvVariable<NodeEnv>('NODE_ENV') === 'production',
  MEDIA_PATH: getEnvVariable('MEDIA_PATH', 'public'),
  VIDEO_FOLDER: getEnvVariable('VIDEO_FOLDER', 'videos'),
  AUDIO_FOLDER: getEnvVariable('AUDIO_FOLDER', 'audios'),
  THUMBNAIL_FOLDER: getEnvVariable('THUMBNAIL_FOLDER', 'thumbnails'),
  DOCUMENTS_FOLDER: getEnvVariable('DOCUMENTS_FOLDER', 'documents'),
  OPENAI_API_KEY: getEnvVariable('OPENAI_API_KEY'),
  IMAGE_FOLDER: getEnvVariable('IMAGE_FOLDER', 'images'),
  FFMPEG_PATH: getEnvVariable('FFMPEG_PATH', 'C:\\ffmpeg\\bin\\ffmpeg.exe'),
  FFPROBE_PATH: getEnvVariable('FFPROBE_PATH', 'C:\\ffmpeg\\bin\\ffprobe.exe'),
  IS_STAGE_ENV: getEnvVariable<NodeEnv>('NODE_ENV') === 'staging',
  IS_DEVELOPMENT_ENV: getEnvVariable<NodeEnv>('NODE_ENV') === 'development',
  IS_SERVER_RUNNING_LOCAL:
    getEnvVariable<`${boolean}`>('LOCAL', 'true') === 'true',
};

export class EnvConfig {
  DATABASE_URL!: string;
  PORT!: number;
  REQUEST_TIMEOUT!: number;
  MODE!: 'DEBUGGING' | 'OTHER';
  NODE_ENV!: NodeEnv;
  MEDIA_PATH!: string;
  IMAGE_FOLDER!: string;
  VIDEO_FOLDER!: string;
  AUDIO_FOLDER!: string;
  THUMBNAIL_FOLDER!: string;
  FFMPEG_PATH!: string;
  OPENAI_API_KEY!: string;
  FFPROBE_PATH!: string;
  DOCUMENTS_FOLDER!: string;
  IS_PRODUCTION_ENV!: boolean;
  IS_STAGE_ENV!: boolean;
  IS_DEVELOPMENT_ENV!: boolean;
  IS_SERVER_RUNNING_LOCAL!: boolean;
}
