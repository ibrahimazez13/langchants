import { ApiProperty } from '@nestjs/swagger';

export class HttpExceptionModel {
  @ApiProperty({ type: Number })
  statusCode!: number;

  @ApiProperty({ type: String })
  message!: string;

  @ApiProperty({ type: String, isArray: true })
  fields!: Array<string>;
}
