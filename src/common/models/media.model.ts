import { AllowedMimeTypes } from '@modules/media/utils/types';

export class MediaModel {
  name: string;
  mime: AllowedMimeTypes;
  constructor(obj: MediaModel) {
    this.name = obj.name;
    this.mime = obj.mime;
  }
  static createInstance(obj: MediaModel) {
    return new MediaModel(obj);
  }
}
