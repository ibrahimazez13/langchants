import path from 'path';

import { AcceptLanguageResolver, I18nOptions } from 'nestjs-i18n';

import { envConfig } from '../../common/utils/env-config';
import { LanguageCode } from '../../common/utils/types';

export const i18nOptions: I18nOptions = {
  fallbackLanguage: LanguageCode['en-US'],
  loaderOptions: {
    path: path.join(process.cwd(), 'src/assets/i18n'),
    watch: true,
  },

  typesOutputPath: path.join(process.cwd(), 'src/generated/i18n.generated.ts'),
  viewEngine: undefined,
  throwOnMissingKey: true,
  logging: envConfig.IS_DEVELOPMENT_ENV,
  resolvers: [AcceptLanguageResolver],
};
