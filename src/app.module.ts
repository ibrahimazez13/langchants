import { Module } from '@nestjs/common';
import { ClsModule } from 'nestjs-cls';
import { I18nModule } from 'nestjs-i18n';

import { LangchantsModule } from '@modules/langchants/langchants.module';
import { SchedulerModule } from '@modules/scheduler/scheduler.module';

import { i18nOptions } from './bootstrap/configs/i18n-options';
import { GlobalModule } from './core/modules/global/global.module';
import { MediaModule } from './modules/media/media.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [
    I18nModule.forRoot(i18nOptions),
    ClsModule.forRoot({
      global: true,
      middleware: { mount: false },
      guard: { mount: true },
      plugins: [],
    }),
    GlobalModule,
    MediaModule,
    LangchantsModule,
    SchedulerModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
