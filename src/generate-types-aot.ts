import { Logger, Module } from '@nestjs/common';
import { CommandFactory } from 'nest-commander';
import { I18nModule } from 'nestjs-i18n';

import { i18nOptions } from './bootstrap/configs/i18n-options';
import { GenerateTypesRunner } from './modules/commander/generate-types.runner';

@Module({
  imports: [
    //  NOTE: Packages modules
    I18nModule.forRoot(i18nOptions),
  ],
  providers: [GenerateTypesRunner],
})
export class AppModule {}

async function generateTypesAot() {
  await CommandFactory.run(AppModule, new Logger());
}

generateTypesAot();
