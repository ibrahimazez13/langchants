import { Logger } from '@nestjs/common';
import { Command, CommandRunner } from 'nest-commander';

@Command({
  name: 'generate-types',
  description: 'generate types data',
  options: { isDefault: true },
})
export class GenerateTypesRunner extends CommandRunner {
  constructor() {
    super();
  }

  async run(
    _inputs: string[],
    _options: Record<string, unknown>,
  ): Promise<void> {
    Logger.log('Run', GenerateTypesRunner.name);
    return;
  }
}
