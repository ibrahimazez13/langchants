import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { CronExpression } from '@nestjs/schedule/dist/enums/cron-expression.enum';

import { LangchantsRepository } from '@modules/langchants/controllers/langchants.repository';
import { LangchantsService } from '@modules/langchants/controllers/langchants.service';

@Injectable()
export class LangChantsSchedulerService {
  // private readonly _logger = new Logger(BookingSchedulerService.name);

  constructor(
    private readonly _langchantsRepository: LangchantsRepository,
    private readonly _langchantsService: LangchantsService,
  ) {}

  @Cron(CronExpression.EVERY_DAY_AT_MIDNIGHT, {
    name: 'Handle Update Products',
    utcOffset: 0,
  })
  async handleUpdateProducts() {
    const products = await this._langchantsRepository.getProducts({
      limit: 10,
      skip: 0,
    });

    const newProducts = await Promise.all(
      products.map(async (product) => {
        const newDescription = await this._langchantsService._promptQuestion({
          name: product.name,
          description: product.description,
          categoryName: product.categoryName,
        });

        return {
          id: product.id,
          newDescription: newDescription,
        };
      }),
    );

    await this._langchantsRepository.updateById(newProducts);
  }
}
