import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';

import { LangchantsRepository } from '@modules/langchants/controllers/langchants.repository';
import { LangchantsService } from '@modules/langchants/controllers/langchants.service';

import { LangChantsSchedulerService } from './lang-chants-scheduler.service';

@Module({
  imports: [ScheduleModule.forRoot()],
  providers: [
    LangChantsSchedulerService,
    LangchantsService,
    LangchantsRepository,
  ],
})
export class SchedulerModule {}
