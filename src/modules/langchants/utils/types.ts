export type LangChantsRow = {
  ItemID: string | null;
  ManufacturerID: string | null;
  ProductID: string;
  ProductName: string;
  ProductDescription: string;
  ManufacturerItemCode: string;
  ItemDescription: string;
  PKG: string;
  UnitPrice: string;
  QuantityOnHand: string | null;
  CategoryName: string | null;
};
