import { IntersectionType } from '@nestjs/swagger';

import { paginationDto } from '@common/dtos/pagination.dto';

import { LANG_CHANTS_MAX_LIMIT } from '../utils/constants';

export class GetAllLangchantsDto extends IntersectionType(
  paginationDto({
    limit: { max: LANG_CHANTS_MAX_LIMIT },
  }),
) {}
