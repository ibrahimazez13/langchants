import { IsMongoId } from 'class-validator';

import { ApiProperty } from '@nestjs/swagger';

export class CreateLangchantsDto {
  @ApiProperty({ type: String })
  @IsMongoId()
  mediaId!: string;
}
