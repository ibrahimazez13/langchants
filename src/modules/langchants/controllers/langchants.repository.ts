import { nanoid } from 'nanoid';

import { Injectable } from '@nestjs/common';

import { Pagination } from '@common/utils/types';

import { GlobalFacade } from '@core/modules/global/services/global.facade';

import { LangChantsRow } from '../utils/types';

@Injectable()
export class LangchantsRepository {
  create = async (data: Array<LangChantsRow>) => {
    const docId = nanoid();
    // const draw = await this._globalFacade.prismaService.draw.create({
    //   data: {
    //     date: toIso8601WithEmptyTime(date),
    //   },

    console.log('docId', docId);
    // });

    await this._globalFacade.prismaService.$transaction(
      data.map(
        ({
          ItemDescription,
          ItemID,
          ManufacturerItemCode,
          ManufacturerID,
          ProductDescription,
          ProductID,
          ProductName,
          PKG,
          CategoryName,
          QuantityOnHand,
          UnitPrice,
        }) => {
          const randomName = Array(8)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('');

          let itemId: number | null = null;

          if (ItemID) {
            const parsedItemId = +ItemID;

            if (!Number.isNaN(parsedItemId)) {
              itemId = parsedItemId;
            }
          }

          let quantity: number = 0;

          if (QuantityOnHand) {
            const parsedQuantityOnHand = +QuantityOnHand;

            if (!Number.isNaN(parsedQuantityOnHand)) {
              quantity = parsedQuantityOnHand;
            }
          }

          return this._globalFacade.prismaService['product'].upsert({
            where: { productId: +ProductID },
            create: {
              manufacturerId: ManufacturerID ? +ManufacturerID : null,
              description: ProductDescription,
              name: ProductName,
              docId: docId,
              itemId: itemId,
              productId: +ProductID,
              categoryName: CategoryName,
              variants: {
                create: {
                  manufacturerItemCode: ManufacturerItemCode,
                  itemDescription: ItemDescription,
                  packaging: PKG,
                  cost: +UnitPrice,
                  quantity: quantity,
                  manufacturerItemId: `${Date.now()}-${randomName}`,
                },
              },
            },
            update: {
              variants: {
                create: {
                  manufacturerItemCode: ManufacturerItemCode,
                  itemDescription: ItemDescription,
                  packaging: PKG,
                  cost: +UnitPrice,
                  quantity: quantity,
                  manufacturerItemId: `${Date.now()}-${randomName}`,
                },
              },
            },
          });
        },
      ),
    );
  };
  constructor(private readonly _globalFacade: GlobalFacade) {}

  getProducts(pagination: Pagination) {
    return this._globalFacade.prismaService['product'].findMany({
      skip: pagination.skip,
      take: pagination.limit,
      select: {
        productId: true,
        description: true,
        docId: true,
        name: true,
        categoryName: true,
        itemId: true,
        variants: true,
        id: true,
        manufacturerId: true,
      },
    });
  }

  async updateById(products: Array<{ id: string; newDescription: string }>) {
    return this._globalFacade.prismaService.$transaction(
      products.map((product) => {
        return this._globalFacade.prismaService.product.update({
          where: { id: product.id },
          data: { description: product.newDescription },
          select: { description: true, id: true },
        });
      }),
    );
  }
}
