import { Injectable } from '@nestjs/common';

import { envConfig } from '@common/utils/env-config';
import { Pagination } from '@common/utils/types';

import { LangchantsRepository } from '@modules/langchants/controllers/langchants.repository';
import { LangChantsRow } from '@modules/langchants/utils/types';
import { ValidateFileTypeService } from '@modules/media/services/validate-file-type.service';
import {
  AllowedMediaTypes,
  ExcelValidationOptionsDetails,
} from '@modules/media/utils/types';
import { MediaPayload } from '@modules/media/validators/select-media.validator';

import { MediaRepository } from '@core/modules/global/repositories/media.repository';
import { GlobalFacade } from '@core/modules/global/services/global.facade';
import { SystemMessage } from '@langchain/core/messages';
import { StringOutputParser } from '@langchain/core/output_parsers';
import { ChatOpenAI } from '@langchain/openai';

@Injectable()
export class LangchantsService {
  constructor(
    private readonly _langchantsRepository: LangchantsRepository,
    private readonly _mediaRepository: MediaRepository,
    private readonly _globalFacade: GlobalFacade,
    private readonly _validateFileTypeService: ValidateFileTypeService,
  ) {}

  async create(media: MediaPayload) {
    const excelSheet =
      await this._validateFileTypeService.validateExcelSheet<LangChantsRow>(
        { fileName: media.name, mime: media.mime as AllowedMediaTypes },
        {
          allowedNames: [
            'SiteSource',
            'ManufacturerCode',
            'ItemID',
            'ManufacturerID',
            'ManufacturerName',
            'ProductID',
            'ProductName',
            'ProductDescription',
            'ManufacturerItemCode',
            'ItemDescription',
            'ImageFileName',
            'NDCItemCode',
            'PKG',
            'UnitPrice',
            'ItemImageURL',
            'QuantityOnHand',
            'PriceDescription',
            'Availability',
            'PrimaryCategoryID',
            'PrimaryCategoryName',
            'SecondaryCategoryID',
            'SecondaryCategoryName',
            'CategoryID',
            'CategoryName',
            'IsRX',
            'IsTBD',
          ],
          canBeEmpty: false,
          details: this._excelValidationOptionsDetails,
        },
        {
          sheetNumberIndex: '+0',
          startingRowIndex: '+0',
          startingColumnIndex: '+1',
        },
      );

    if (!excelSheet.isValid) {
      return excelSheet;
    }

    await this._langchantsRepository.create(excelSheet.data);

    return excelSheet;
  }

  getProducts(pagination: Pagination) {
    return this._langchantsRepository.getProducts(pagination);
  }
  async updateById(products: Array<{ id: string; newDescription: string }>) {
    return this._langchantsRepository.updateById(products);
  }
  get _excelValidationOptionsDetails() {
    return [
      // {
      //   name: 'SiteSource',
      //   condition: {
      //     type: 'STRING',
      //     subTypes: [],
      //   },
      //   allowDuplicates: true,
      // },
      {
        name: 'ItemID',
        condition: {
          type: 'NUMBER',
          subTypes: [{ type: 'NULLABLE' }],
        },
        allowDuplicates: false,
      },
      {
        name: 'ManufacturerID',
        condition: {
          type: 'NUMBER',
          subTypes: [{ type: 'NULLABLE' }],
        },
        allowDuplicates: true,
      },
      {
        name: 'ManufacturerCode',
        condition: {
          type: 'STRING',
          subTypes: [],
        },
        allowDuplicates: true,
      },
      {
        name: 'ManufacturerName',
        condition: {
          type: 'STRING',
          subTypes: [{ type: 'NULLABLE' }],
        },
        allowDuplicates: true,
      },
      {
        name: 'ProductID',
        condition: {
          type: 'NUMBER',
          subTypes: [],
        },
        allowDuplicates: true,
      },
      {
        name: 'ProductName',
        condition: {
          type: 'STRING',
          subTypes: [],
        },
        allowDuplicates: true,
      },
      {
        name: 'ProductDescription',
        condition: {
          type: 'STRING',
          subTypes: [{ type: 'NULLABLE' }],
        },
        allowDuplicates: true,
      },
      {
        name: 'ManufacturerItemCode',
        condition: {
          type: 'STRING',
          subTypes: [{ type: 'NULLABLE' }],
        },
        allowDuplicates: true,
      },
      {
        name: 'ItemImageURL',
        condition: {
          type: 'STRING',
          subTypes: [{ type: 'NULLABLE' }],
        },
        allowDuplicates: true,
      },
      {
        name: 'ItemDescription',
        condition: {
          type: 'STRING',
          subTypes: [{ type: 'NULLABLE' }],
        },
        allowDuplicates: true,
      },
      {
        name: 'ImageFileName',
        condition: {
          type: 'STRING',
          subTypes: [{ type: 'NULLABLE' }],
        },
        allowDuplicates: true,
      },
      // {
      //   name: 'NDCItemCode',
      //   condition: {
      //     type: 'STRING',
      //     subTypes: [{ type: 'NULLABLE' }],
      //   },
      //   allowDuplicates: true,
      // },
      {
        name: 'PKG',
        condition: {
          type: 'STRING',
          subTypes: [{ type: 'NULLABLE' }],
        },
        allowDuplicates: true,
      },

      {
        name: 'UnitPrice',
        condition: {
          type: 'NUMBER',
          subTypes: [],
        },
        allowDuplicates: true,
      },
      {
        name: 'QuantityOnHand',
        condition: {
          type: 'NUMBER',
          subTypes: [{ type: 'MIN', limit: 0 }, { type: 'NULLABLE' }],
        },
        allowDuplicates: true,
      },

      {
        name: 'PriceDescription',
        condition: {
          type: 'STRING',
          subTypes: [{ type: 'NULLABLE' }],
        },
        allowDuplicates: true,
      },
      {
        name: 'Availability',
        condition: {
          type: 'STRING',
          subTypes: [{ type: 'NULLABLE' }],
        },
        allowDuplicates: true,
      },
      // {
      //   name: 'PrimaryCategoryID',
      //   condition: {
      //     type: 'NUMBER',
      //     subTypes: [{ type: 'NULLABLE' }],
      //   },
      //   allowDuplicates: true,
      // },
      // {
      //   name: 'PrimaryCategoryName',
      //   condition: {
      //     type: 'STRING',
      //     subTypes: [{ type: 'NULLABLE' }],
      //   },
      //   allowDuplicates: true,
      // },
      // {
      //   name: 'SecondaryCategoryID',
      //   condition: {
      //     type: 'NUMBER',
      //     subTypes: [{ type: 'NULLABLE' }],
      //   },
      //   allowDuplicates: true,
      // },
      // {
      //   name: 'SecondaryCategoryName',
      //   condition: {
      //     type: 'STRING',
      //     subTypes: [{ type: 'NULLABLE' }],
      //   },
      //   allowDuplicates: true,
      // },
      {
        name: 'CategoryID',
        condition: {
          type: 'NUMBER',
          subTypes: [],
        },
        allowDuplicates: true,
      },
      {
        name: 'CategoryName',
        condition: {
          type: 'STRING',
          subTypes: [{ type: 'NULLABLE' }],
        },
        allowDuplicates: true,
      },
      // {
      //   name: 'IsRX',
      //   condition: {
      //     type: 'ENUM',
      //     subTypes: [{ type: 'NULLABLE' }],
      //     values: ['Y', 'N'],
      //   },
      //   allowDuplicates: true,
      // },
      // {
      //   name: 'IsTBD',
      //   condition: {
      //     type: 'ENUM',
      //     subTypes: [{ type: 'NULLABLE' }],
      //     values: ['Y', 'N'],
      //   },
      //   allowDuplicates: true,
      // },
    ] satisfies ExcelValidationOptionsDetails;
  }

  async _promptQuestion(product: {
    name: string;
    description: string | null;
    categoryName: string | null;
  }) {
    const model = new ChatOpenAI({
      apiKey: envConfig.OPENAI_API_KEY,
      temperature: 0,
      modelName: 'gpt-4',
    });

    const messages = [
      new SystemMessage(`
      You are an expert in medical sales. Your specialty is medical consumables used by hospitals on a daily basis. Your task to enhance the description of a product based on the information provided.

      Product name: ${product.name}
      Product description: ${product.description ?? '$description'}
      Category: ${product.categoryName ?? '$categoryName'}
      
      New Description:
`),
    ];

    const parser = new StringOutputParser();
    const result = await model.invoke(messages);
    const parsed = await parser.invoke(result);

    console.log(parsed);
    return parsed;
  }
}
