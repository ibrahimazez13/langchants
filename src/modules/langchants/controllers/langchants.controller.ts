import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  NotFoundException,
  Post,
  Query,
  UseInterceptors,
  VERSION_NEUTRAL,
} from '@nestjs/common';
import { ApiTags, IntersectionType, OmitType } from '@nestjs/swagger';

import { HttpExceptionModel } from '@common/models/http-exception.model';

import { LangchantsService } from '@modules/langchants/controllers/langchants.service';
import { CreateLangchantsDto } from '@modules/langchants/dtos/create-langchants.dto';
import { GetAllLangchantsDto } from '@modules/langchants/dtos/get-all-langchants.dto';
import { ExcelHttpExceptionModel } from '@modules/media/models/excel-http-exception.model';
import { selectMediaValidator } from '@modules/media/validators/select-media.validator';

import { MyApiResponse } from '@core/decorators/swagger/my-api-response.decorator';
import { TimeoutInterceptor } from '@core/interceptors/timeout.interceptor';
import { GlobalFacade } from '@core/modules/global/services/global.facade';
import { PrismaService } from '@core/modules/global/services/prisma.service';

@Controller({ path: 'langchants', version: VERSION_NEUTRAL })
@ApiTags('Langchants')
export class LangchantsController {
  constructor(
    private readonly _globalFacade: GlobalFacade,
    private readonly _langchantsService: LangchantsService,
    private readonly _prismaService: PrismaService,
  ) {}

  @MyApiResponse({
    conflicted: true,
    notFound: true,
    badRequest: {
      type: IntersectionType(
        OmitType(HttpExceptionModel, ['fields'] as const),
        ExcelHttpExceptionModel,
      ),
    },
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Post('')
  @UseInterceptors(TimeoutInterceptor)
  async create(@Body() createLangchantsDto: CreateLangchantsDto) {
    const media = await this._globalFacade.prismaService.media.findUnique({
      where: { id: createLangchantsDto.mediaId },
      select: selectMediaValidator,
    });

    if (!media) {
      throw new NotFoundException();
    }

    const file = await this._langchantsService.create(media);

    if (!file.isValid && file.message !== null) {
      throw new BadRequestException({
        message: file.message,
      });
    }

    if (!file.isValid) {
      throw new BadRequestException(
        ExcelHttpExceptionModel.instanceToPlain(file.errors),
      );
    }

    return;
  }

  @MyApiResponse({
    ok: {},
  })
  @Get('')
  async getAll(@Query() getAllLangchantsDto: GetAllLangchantsDto) {
    return this._langchantsService.getProducts(getAllLangchantsDto.pagination);
  }

  @MyApiResponse({
    ok: {},
  })
  @Get('prompt')
  async promptQuestion() {
    const products = await this._langchantsService.getProducts({
      limit: 2,
      skip: 0,
    });

    const newProducts = await Promise.all(
      products.map(async (product) => {
        const newDescription = await this._langchantsService._promptQuestion({
          name: product.name,
          description: product.description,
          categoryName: product.categoryName,
        });

        return {
          id: product.id,
          newDescription: newDescription,
        };
      }),
    );

    return await this._langchantsService.updateById(newProducts);
  }
}
