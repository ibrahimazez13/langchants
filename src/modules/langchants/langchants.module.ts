import { Module } from '@nestjs/common';

import { LangchantsController } from '@modules/langchants/controllers/langchants.controller';
import { LangchantsRepository } from '@modules/langchants/controllers/langchants.repository';
import { LangchantsService } from '@modules/langchants/controllers/langchants.service';

@Module({
  controllers: [LangchantsController],
  providers: [LangchantsService, LangchantsRepository],
  exports: [],
})
export class LangchantsModule {}
