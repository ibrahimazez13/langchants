import { Prisma } from '@prisma/client';

export const selectMediaValidator = Prisma.validator<Prisma.MediaSelect>()({
  id: true,
  url: true,
  duration: true,
  width: true,
  height: true,
  dominantColor: true,
  ext: true,
  type: true,
  aspectRatio: true,
  thumbnailUrl: true,
  name: true,
  mime: true,
});

export type MediaPayload = Prisma.MediaGetPayload<{
  select: typeof selectMediaValidator;
}>;
