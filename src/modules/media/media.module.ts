import { Global, Module } from '@nestjs/common';

import { MediaController } from './controllers/media.controller';
import { MediaService } from './controllers/media.service';
import { CheckIfColumnsHasDuplicationInValues } from './services/check-if-columns-has-duplication-in-values.service';
import { DoesFileValueHasTheRightType } from './services/does-file-value-has-the-right-type.service';
import { ValidateFileTypeService } from './services/validate-file-type.service';

@Global()
@Module({
  controllers: [MediaController],
  providers: [
    MediaService,
    ValidateFileTypeService,
    CheckIfColumnsHasDuplicationInValues,
    DoesFileValueHasTheRightType,
  ],
  exports: [
    MediaService,
    ValidateFileTypeService,
    CheckIfColumnsHasDuplicationInValues,
    DoesFileValueHasTheRightType,
  ],
})
export class MediaModule {}

// BullModule.registerQueue({
//   name: MEDIA_QUEUE,
//   defaultJobOptions: { removeOnComplete: true, removeOnFail: true },
// }),
