import {
  ErrorMapCtx,
  z,
  ZodBoolean,
  ZodEffects,
  ZodEnum,
  ZodIssueCode,
  ZodIssueOptionalMessage,
  ZodNullable,
  ZodNumber,
  ZodPipeline,
  ZodString,
  ZodTypeAny,
} from 'zod';

import { Injectable } from '@nestjs/common';

import { MONGO_ID_REGEX } from '../../../common/utils/regex';
import { GlobalFacade } from '../../../core/modules/global/services/global.facade';
import {
  DatabaseQuery,
  ExcelColumnValueValidationError,
  ExcelValidationOptionsDetails,
  FileValidationCondition,
  ValidateConditionResult,
} from '../utils/types';

@Injectable()
export class DoesFileValueHasTheRightType {
  constructor(private readonly _globalFacade: GlobalFacade) {}
  async validateValues(obj: {
    item: Record<string, unknown>;
    details: Omit<ExcelValidationOptionsDetails, 'allowDuplicates'>;
    columns: Record<string, Array<unknown>>;
  }) {
    const { item, details } = obj;
    let validationErrors: Array<ExcelColumnValueValidationError> = [];

    await Promise.all(
      details.map(async ({ name, condition }) => {
        let value: unknown = null;

        if (item[name]) {
          value = item[name];
        }

        let conditionsErrors: ExcelColumnValueValidationError | null = null;

        // const columnValues = obj.columns[name];

        const validateConditionResult = await this._validateVerboseCondition({
          value: value,
          condition: condition,
        });

        const isValid = validateConditionResult.isValid;
        const parsedValue = validateConditionResult.parsedValue;
        const errors = validateConditionResult.errors;

        if (!isValid) {
          conditionsErrors = {
            name: name,
            issues: errors,
            value: parsedValue,
          };
        }

        if (conditionsErrors) {
          validationErrors = [...validationErrors, conditionsErrors];
        }
      }),
    );

    return validationErrors;
  }

  private async _validateVerboseCondition(obj: {
    value: unknown;
    condition: FileValidationCondition;
  }): Promise<ValidateConditionResult> {
    const { condition, value } = obj;
    const { type, databaseQuery } = condition;

    switch (type) {
      case 'NUMBER': {
        const { subTypes } = condition;
        let zod: ZodNullable<ZodNumber> | ZodNumber = z.number({
          errorMap: (_issue: ZodIssueOptionalMessage, _ctx: ErrorMapCtx) => {
            return {
              message: this._globalFacade
                .translate('error.400.INVALID_NUMBER')
                .toString(),
            };
          },
        });
        subTypes.forEach((subType) => {
          const { type } = subType;

          if (type === 'MAX') {
            const { limit } = subType;
            zod = (zod as ZodNumber).max(limit, {
              message: this._globalFacade
                .translate('error.400.NUMBER_MUST_BE_LESS_THAN', {
                  args: { value: limit },
                })
                .toString(),
            });
          }

          if (type === 'MIN') {
            const { limit } = subType;
            zod = (zod as ZodNumber).min(limit, {
              message: this._globalFacade
                .translate('error.400.NUMBER_MUST_BE_GREATER_THAN', {
                  args: { value: limit },
                })
                .toString(),
            });
          }

          if (type === 'NULLABLE') {
            zod = (zod as ZodNumber).nullable();
          }
        });

        let parsedValue: Nullable<number> = Number(value);
        if (parsedValue && isNaN(parsedValue)) {
          parsedValue = null;
        }

        return this._databaseQueryValidation({
          databaseQuery,
          zod,
          parsedValue,
        });
      }

      case 'STRING': {
        const { subTypes } = condition;
        let zod:
          | ZodPipeline<ZodEffects<ZodString, number, string>, ZodNumber>
          | ZodNullable<ZodString>
          | ZodString = z.string({});
        subTypes.forEach((subType) => {
          const { type } = subType;

          if (type === 'MAX_LENGTH') {
            const { maxLength } = subType;
            zod = (zod as ZodString).max(parseInt(maxLength), {
              message: this._globalFacade
                .translate('error.400.ITEM_MUST_CONTAINS_AT_MOST_CHARACTERS', {
                  args: { value: maxLength },
                })
                .toString(),
            });
          }

          if (type === 'MIN_LENGTH') {
            const { minLength } = subType;
            zod = (zod as ZodString).min(parseInt(minLength), {
              message: this._globalFacade
                .translate('error.400.ITEM_MUST_CONTAINS_AT_LEAST_CHARACTERS', {
                  args: { value: minLength },
                })
                .toString(),
            });
          }

          if (type === 'DATE_TIME') {
            const { options } = subType;
            zod = (zod as ZodString).datetime({
              message: this._globalFacade
                .translate('error.400.INVALID_DATE', {
                  args: { value: value },
                })
                .toString(),
              ...options,
            });
          }

          if (type === 'NUMERIC') {
            zod = (zod as ZodString)
              .transform((x) => Number(x))
              .pipe(z.number());
          }

          if (type === 'UUID') {
            zod = (zod as ZodString).uuid({
              message: this._globalFacade
                .translate('error.400.INVALID_ID')
                .toString(),
            });
          }

          if (type === 'MONGO_ID') {
            zod = (zod as ZodString)
              .regex(MONGO_ID_REGEX, {
                message: this._globalFacade
                  .translate('error.400.INVALID_ID')
                  .toString(),
              })
              .length(24, {
                message: this._globalFacade
                  .translate('error.400.INVALID_ID')
                  .toString(),
              });
          }

          if (type === 'EMAIL') {
            zod = (zod as ZodString).email({
              message: this._globalFacade
                .translate('error.400.INVALID_EMAIL')
                .toString(),
            });
          }

          if (type === 'NULLABLE') {
            zod = (zod as ZodString).nullable();
          }
        });

        const parsedValue = value ? String(value) : null;

        return this._databaseQueryValidation({
          databaseQuery,
          zod,
          parsedValue,
        });
      }

      case 'BOOLEAN': {
        const { subTypes } = condition;
        let zod: ZodNullable<ZodBoolean> | ZodBoolean = z.boolean({
          errorMap: (_issue: ZodIssueOptionalMessage, _ctx: ErrorMapCtx) => {
            return {
              message: this._globalFacade
                .translate('error.400.INVALID_BOOLEAN')
                .toString(),
            };
          },
        });
        subTypes.forEach((subType) => {
          const { type } = subType;

          if (type === 'NULLABLE') {
            zod = (zod as ZodBoolean).nullable();
          }
        });

        const parsedValue = value ? String(value) === 'true' : null;
        return this._databaseQueryValidation({
          databaseQuery,
          zod,
          parsedValue,
        });
      }

      case 'ENUM': {
        const { values, subTypes } = condition;
        let zod:
          | ZodNullable<ZodEnum<[string, ...string[]]>>
          | ZodEnum<[string, ...string[]]> = z.enum(values, {
          errorMap: (_issue: ZodIssueOptionalMessage, _ctx: ErrorMapCtx) => {
            return {
              message: this._globalFacade
                .translate('error.400.INVALID_ENUM')
                .toString(),
            };
          },
        });
        subTypes.forEach((subType) => {
          const { type } = subType;

          if (type === 'NULLABLE') {
            zod = (zod as ZodEnum<[string, ...string[]]>).nullable();
          }
        });

        const parsedValue = value ? String(value) : null;

        return this._databaseQueryValidation({
          databaseQuery,
          zod,
          parsedValue,
        });
      }
    }

    // throw new Error('Code must end with switch case type');
  }
  private async _databaseQueryValidation(obj: {
    databaseQuery: DatabaseQuery | undefined;
    zod: ZodTypeAny;
    parsedValue: Nullable<string | number | boolean>;
  }): Promise<ValidateConditionResult> {
    const { databaseQuery, parsedValue, zod } = obj;
    const result = parsedValue
      ? zod.safeParse(parsedValue)
      : { success: true, error: { issues: [] } };
    const isValid = result.success;

    const validateConditionResult: ValidateConditionResult = {
      isValid: isValid,
      parsedValue: parsedValue,
      errors: result.success ? [] : result.error.issues,
    };

    if (!isValid || !databaseQuery || !parsedValue) {
      return validateConditionResult;
    }

    const model = await databaseQuery(parsedValue);

    return {
      isValid: !!model,
      parsedValue: parsedValue,
      errors: model
        ? []
        : [
            {
              code: ZodIssueCode.custom,
              message: this._globalFacade
                .translate('error.404.ITEM_NOT_FOUND')
                .toString(),
            },
          ],
    };
  }
}
