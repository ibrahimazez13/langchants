import { isNumber } from 'class-validator';
import { isNull } from 'lodash';
import { join } from 'path';
import reader, { Range, Sheet2JSONOpts, utils } from 'xlsx';

import { BadRequestException, Injectable } from '@nestjs/common';

import { envConfig } from '../../../common/utils/env-config';
import { GlobalFacade } from '../../../core/modules/global/services/global.facade';
import { extractImageMetadata } from '../../../modules/media/utils/extract-image-metadata';
import { extractVideoMetadata } from '../../../modules/media/utils/extract-video-metadata';
import { checkExcelColumnNamesIfAllowed } from '../utils/check-excel-file-column-names-if-allowed';
import {
  AllowedMimeDocsList,
  AllowedMimeImageList,
  AllowedMimeVideoList,
} from '../utils/constants';
import { getFileType } from '../utils/get-file-type';
import {
  AllowedMimeTypes,
  ExcelValidationError,
  ExcelValidationOptions,
  ValidateExcelSheetResult,
  ValidateFileTypeResult,
} from '../utils/types';

import { CheckIfColumnsHasDuplicationInValues } from './check-if-columns-has-duplication-in-values.service';
import { DoesFileValueHasTheRightType } from './does-file-value-has-the-right-type.service';

@Injectable()
export class ValidateFileTypeService {
  constructor(
    private readonly _globalFacade: GlobalFacade,
    private readonly _doesFileValueHasTheRightType: DoesFileValueHasTheRightType,
    private readonly _checkIfColumnsHasDuplicationInValues: CheckIfColumnsHasDuplicationInValues,
  ) {}

  async validateFile(filePath: string): Promise<ValidateFileTypeResult> {
    const { fileTypeFromFile } = await getFileType();
    const fileTypeResult = await fileTypeFromFile(filePath);

    if (fileTypeResult === undefined) {
      throw new BadRequestException(
        this._globalFacade.translate('error.400.PLEASE_SEND_A_VALID_FILE'),
      );
    }

    const imageMimeType = AllowedMimeImageList.find(
      (value) => value === fileTypeResult.mime,
    );

    const videoMimeType = AllowedMimeVideoList.find(
      (value) => value === fileTypeResult.mime,
    );

    const DocumentMimeType = AllowedMimeDocsList.find(
      (value) => value === fileTypeResult.mime,
    );

    if (imageMimeType !== undefined) {
      return extractImageMetadata({
        mime: fileTypeResult.mime,
        ext: fileTypeResult.ext,
        path: filePath,
      });
    }

    if (videoMimeType !== undefined) {
      return extractVideoMetadata({
        mime: fileTypeResult.mime,
        ext: fileTypeResult.ext,
        path: filePath,
      });
    }

    if (DocumentMimeType) {
      return {
        mimeType: DocumentMimeType,
        type: 'DOCUMENT',

        duration: undefined,
        aspectRatio: undefined,
        codecName: undefined,
        space: undefined,
        gcd: undefined,
      };
    }

    throw new BadRequestException(
      this._globalFacade.translate('error.400.PLEASE_SEND_A_VALID_FILE'),
    );
  }

  async validateExcelSheet<
    T extends Record<string, number | string | boolean | null>,
  >(
    candidateFile: { fileName: string; mime: AllowedMimeTypes },
    validationOptions: ExcelValidationOptions,
    readingOptions: {
      sheetNumberIndex: `+${number}`;
      startingRowIndex: `+${number}`;
      startingColumnIndex: `+${number}`;
      excludeRowIndex?: `+${number}`;
    },
  ): Promise<ValidateExcelSheetResult<T>> {
    if (
      candidateFile.mime !==
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    ) {
      return {
        isValid: false,
        message: this._globalFacade
          .translate('error.422.MEDIA_ID_MUST_BE_AN_EXCEL_SHEET_MEDIA_ID')
          .toString(),
        errors: [],
      };
    }

    const sheetNumberIndex = parseInt(readingOptions.sheetNumberIndex);
    const startingRowIndex = parseInt(readingOptions.startingRowIndex);
    const startingColumnIndex = parseInt(readingOptions.startingColumnIndex);
    const excludeRowIndex =
      readingOptions.excludeRowIndex &&
      parseInt(readingOptions.excludeRowIndex);

    const file = reader.readFile(
      join(
        process.cwd(),
        envConfig.MEDIA_PATH,
        envConfig.DOCUMENTS_FOLDER,
        candidateFile.fileName,
      ),
    );

    let validationError: ExcelValidationError = {
      columnNames: [],
      columnValues: [],
    };

    const workSheet = file.Sheets[file.SheetNames[sheetNumberIndex]];

    const rangeString = workSheet['!ref'];

    if (!rangeString) {
      return {
        isValid: false,
        message: this._globalFacade
          .translate('error.400.PLEASE_SEND_A_VALID_DOCUMENT')
          .toString(),
        errors: [],
      };
    }

    const range = utils.decode_range(rangeString);
    const options: Omit<Sheet2JSONOpts, 'range'> & { range: Range } = {
      range: { ...range, s: { c: startingColumnIndex, r: startingRowIndex } },
      raw: false,
      /*
       * To add column name if value does not exist
       * without it, it would be
       * { columnName2: <some value> }
       * with defval of null it would be
       * { columnName: null, columnName2: <some value> }
       * If the file has only one column, defval won't work.
       * The column will be stripped at all, and it there won't be
       * null value like this { columnName: null } and not even {}
       * the solution for this is to add blank rows to true
       */
      defval: null,
      blankrows: true,
    };

    const data: Array<T> = reader.utils.sheet_to_json(
      file.Sheets[file.SheetNames[sheetNumberIndex]],
      options,
    );

    // If array is empty, then there is no need to continue
    if (data.length === 0) {
      if (validationOptions.canBeEmpty) {
        return {
          isValid: false,
          errors: [],
          message: this._globalFacade
            .translate('error.400.FILE_CAN_NOT_BE_EMPTY')
            .toString(),
        };
      }

      return { isValid: true, data: data, errors: null };
    }

    /*
     * Make sure the data length contains the
     * row needed to be excluded
     *
     */
    if (isNumber(excludeRowIndex) && data.length > excludeRowIndex) {
      // only splice array when item is found
      data.splice(excludeRowIndex, 1); // 2nd parameter means remove one item only
    }

    // Check column names if valid, if it is not there is no need to continue
    const dataColumnNames = Object.keys(data[0]);
    const columnNameErrors = checkExcelColumnNamesIfAllowed({
      dataColumnNames: dataColumnNames,
      allowedNames: validationOptions.allowedNames,
    });

    if (columnNameErrors.length) {
      validationError = {
        ...validationError,
        columnNames: [...columnNameErrors, ...validationError.columnNames],
      };

      return {
        isValid: false,
        errors: validationError,
        message: null,
      };
    }

    /*
     * Extract only the column that needs validation for duplication,
     * Set value to be empty array to be filled with the values later on.
     */
    let columns: Record<string, Array<number | string | boolean | null>> = {};
    validationOptions.details.forEach(({ name, allowDuplicates }) => {
      if (!allowDuplicates) {
        columns = { ...columns, [name]: [] };
      }
    });
    /**
     *  We need to Validate data rows, each row must accommodate the correct value type.
     *
     *  NOTE:
     *  Because the data may be too big, passing it the data to any extracted method
     *  would create big load on memory, so It is better to validate values in the same
     *  method.
     *
     */

    await Promise.all(
      data.map(async (item) => {
        validationOptions.allowedNames.forEach((name) => {
          if (!isNull(item[name])) {
            /* column will be empty {} if allowDuplicates is set to true
             * so We need to use nullish coalescing
             */
            if (columns[name]) {
              columns = {
                ...columns,
                [name]: [...columns[name], item[name]],
              };
            }
          }
        });
        const validateValuesResult =
          await this._doesFileValueHasTheRightType.validateValues({
            item: item,
            details: validationOptions.details,
            columns: columns,
          });
        const excelColumnValueValidationError = [...validateValuesResult];

        validationError = {
          ...validationError,
          columnValues: [
            ...excelColumnValueValidationError,
            ...validationError.columnValues,
          ],
        };
      }),
    );
    if (validationError.columnValues.length > 0) {
      return {
        isValid: false,
        errors: validationError,
        message: null,
      };
    }
    const columnsDuplicationErrors =
      this._checkIfColumnsHasDuplicationInValues.validate(columns);

    if (columnsDuplicationErrors.length > 0) {
      return {
        isValid: false,
        errors: {
          ...validationError,
          columnValues: [
            ...validationError.columnValues,
            ...columnsDuplicationErrors,
          ],
        },
        message: null,
      };
    }

    return { isValid: true, data: data, errors: null };
  }
}
