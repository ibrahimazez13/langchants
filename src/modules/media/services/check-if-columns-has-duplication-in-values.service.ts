import { Injectable } from '@nestjs/common';

import { GlobalFacade } from '../../../core/modules/global/services/global.facade';
import { getDuplicatedItemsInArray } from '../utils/get-duplicated-items-in-array';
import { ExcelColumnValueValidationError } from '../utils/types';

@Injectable()
export class CheckIfColumnsHasDuplicationInValues {
  constructor(private readonly _globalFacade: GlobalFacade) {}

  validate(columns: Record<string, Array<number | string | boolean | null>>) {
    const columnsDuplicationErrors: Array<ExcelColumnValueValidationError> = [];

    /*
     * Validate if file columns can not have duplicate values
     */
    Object.entries(columns).forEach(([key, values]) => {
      const duplicatedItems = getDuplicatedItemsInArray(values);

      duplicatedItems.forEach((item) => {
        columnsDuplicationErrors.push({
          name: key,
          value: item,
          issues: [
            {
              code: 'invalid_literal',
              message: this._globalFacade
                .translate('error.400.DUPLICATED_VALUE')
                .toString(),
            },
          ],
        });
      });
    });

    return columnsDuplicationErrors;
  }
}
