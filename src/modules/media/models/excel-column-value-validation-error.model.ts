import { ClassTransformOptions, instanceToPlain } from 'class-transformer';

import { ApiProperty } from '@nestjs/swagger';

import { ExcelColumnValueValidationError } from '@modules/media/utils/types';

import { FileValidationConditionModel } from './file-validation-condition.model';

type Payload = ExcelColumnValueValidationError;

export class ExcelColumnValueValidationErrorModel {
  @ApiProperty({ type: String }) name: string;
  @ApiProperty({
    oneOf: [{ type: 'string' }, { type: 'number' }, { type: 'boolean' }],
  })
  value: Nullable<string | number | boolean>;
  @ApiProperty({
    type: FileValidationConditionModel,
    isArray: true,
  })
  issues: Array<FileValidationConditionModel>;
  constructor(obj: Payload) {
    const { name, value, issues } = obj;

    this.name = name;
    this.value = value;
    this.issues = FileValidationConditionModel.createInstance(issues);
  }
  static instanceToPlain(
    obj: Payload[],
    options?: ClassTransformOptions,
  ): ArrayOfEntities;

  static instanceToPlain(
    obj: Payload,
    options?: ClassTransformOptions,
  ): EntityRecord;

  static instanceToPlain(
    obj: MaybeArray<Payload>,
    options?: ClassTransformOptions,
  ) {
    if (Array.isArray(obj)) {
      return obj.map((item) => instanceToPlain(new this(item), options));
    } else {
      return instanceToPlain(new this(obj), options);
    }
  }

  static createInstance(obj: Payload): ExcelColumnValueValidationErrorModel;
  static createInstance(obj: Payload[]): ExcelColumnValueValidationErrorModel[];
  static createInstance(obj: MaybeArray<Payload>) {
    if (Array.isArray(obj)) {
      return obj.map((item) => new this(item));
    }
    return new this(obj);
  }
}
