import { ClassTransformOptions, instanceToPlain } from 'class-transformer';

import { ApiProperty } from '@nestjs/swagger';

import { ExcelValidationError } from '@modules/media/utils/types';

import { ExcelValidationErrorModel } from './excel-validation-error.model';

type Payload = ExcelValidationError;

export class ExcelHttpExceptionModel {
  @ApiProperty({ type: ExcelValidationErrorModel })
  fields!: ExcelValidationErrorModel;
  constructor(obj: Payload) {
    this.fields = ExcelValidationErrorModel.createInstance(obj);
  }
  static instanceToPlain(
    obj: Payload[],
    options?: ClassTransformOptions,
  ): ArrayOfEntities;

  static instanceToPlain(
    obj: Payload,
    options?: ClassTransformOptions,
  ): EntityRecord;
  static instanceToPlain(
    obj: MaybeArray<Payload>,
    options?: ClassTransformOptions,
  ) {
    if (Array.isArray(obj)) {
      return obj.map((item) => instanceToPlain(new this(item), options));
    } else {
      return instanceToPlain(new this(obj), options);
    }
  }

  static createInstance(obj: Payload): ExcelHttpExceptionModel;
  static createInstance(obj: Payload[]): ExcelHttpExceptionModel[];
  static createInstance(obj: MaybeArray<Payload>) {
    if (Array.isArray(obj)) {
      return obj.map((item) => new this(item));
    }
    return new this(obj);
  }
}
