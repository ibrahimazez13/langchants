import { ClassTransformOptions, instanceToPlain } from 'class-transformer';

import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

import {
  ExcelColumnNameValidationError,
  ExcelColumnNameValidationErrorReason,
} from '@modules/media/utils/types';

type Payload = ExcelColumnNameValidationError;
export class ExcelColumnNameValidationErrorModel {
  @ApiPropertyOptional({ type: String }) name: Nullable<string>;
  @ApiProperty({
    isArray: true,
    type: String,
  })
  allowedNames: Array<string>;
  @ApiProperty({
    type: String,
    enum: ExcelColumnNameValidationErrorReason,
  })
  reason: ExcelColumnNameValidationErrorReason;
  constructor(obj: Payload) {
    const { name, allowedNames, reason } = obj;

    this.name = name;
    this.allowedNames = allowedNames;
    this.reason = reason;
  }
  static instanceToPlain(
    obj: Payload[],
    options?: ClassTransformOptions,
  ): ArrayOfEntities;

  static instanceToPlain(
    obj: Payload,
    options?: ClassTransformOptions,
  ): EntityRecord;

  static instanceToPlain(
    obj: MaybeArray<Payload>,
    options?: ClassTransformOptions,
  ) {
    if (Array.isArray(obj)) {
      return obj.map((item) => instanceToPlain(new this(item), options));
    } else {
      return instanceToPlain(new this(obj), options);
    }
  }

  static createInstance(obj: Payload): ExcelColumnNameValidationErrorModel;
  static createInstance(obj: Payload[]): ExcelColumnNameValidationErrorModel[];
  static createInstance(obj: MaybeArray<Payload>) {
    if (Array.isArray(obj)) {
      return obj.map((item) => new this(item));
    }
    return new this(obj);
  }
}
