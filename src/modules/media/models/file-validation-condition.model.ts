import { ClassTransformOptions, instanceToPlain } from 'class-transformer';
import { ZodIssueCode } from 'zod';

import { ApiProperty } from '@nestjs/swagger';

import { ValidateConditionResultError } from '@modules/media/utils/types';

type Payload = ValidateConditionResultError;

export class FileValidationConditionModel {
  @ApiProperty({ type: String })
  message: string;
  @ApiProperty({
    type: String,
    enum: ZodIssueCode,
  })
  code: ZodIssueCode;
  constructor(obj: Payload) {
    const { message, code } = obj;

    this.code = code;
    this.message = message;
  }
  static instanceToPlain(
    obj: Payload[],
    options?: ClassTransformOptions,
  ): ArrayOfEntities;

  static instanceToPlain(
    obj: Payload,
    options?: ClassTransformOptions,
  ): EntityRecord;

  static instanceToPlain(
    obj: MaybeArray<Payload>,
    options?: ClassTransformOptions,
  ) {
    if (Array.isArray(obj)) {
      return obj.map((item) => instanceToPlain(new this(item), options));
    } else {
      return instanceToPlain(new this(obj), options);
    }
  }

  static createInstance(obj: Payload): FileValidationConditionModel;
  static createInstance(obj: Payload[]): FileValidationConditionModel[];
  static createInstance(obj: MaybeArray<Payload>) {
    if (Array.isArray(obj)) {
      return obj.map((item) => new this(item));
    }
    return new this(obj);
  }
}
