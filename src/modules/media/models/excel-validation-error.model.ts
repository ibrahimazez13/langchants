import { ClassTransformOptions, instanceToPlain } from 'class-transformer';

import { ApiProperty } from '@nestjs/swagger';

import { ExcelValidationError } from '@modules/media/utils/types';

import { ExcelColumnNameValidationErrorModel } from './excel-column-name-validation-error.model';
import { ExcelColumnValueValidationErrorModel } from './excel-column-value-validation-error.model';

type Payload = ExcelValidationError;
export class ExcelValidationErrorModel {
  @ApiProperty({ type: ExcelColumnNameValidationErrorModel, isArray: true })
  columnNames: Array<ExcelColumnNameValidationErrorModel>;
  @ApiProperty({
    type: ExcelColumnValueValidationErrorModel,
    isArray: true,
  })
  columnValues: Array<ExcelColumnValueValidationErrorModel>;
  constructor(obj: Payload) {
    const { columnNames, columnValues } = obj;

    this.columnNames =
      ExcelColumnNameValidationErrorModel.createInstance(columnNames);
    this.columnValues =
      ExcelColumnValueValidationErrorModel.createInstance(columnValues);
  }
  static instanceToPlain(
    obj: Payload[],
    options?: ClassTransformOptions,
  ): ArrayOfEntities;

  static instanceToPlain(
    obj: Payload,
    options?: ClassTransformOptions,
  ): EntityRecord;

  static instanceToPlain(
    obj: MaybeArray<Payload>,
    options?: ClassTransformOptions,
  ) {
    if (Array.isArray(obj)) {
      return obj.map((item) => instanceToPlain(new this(item), options));
    } else {
      return instanceToPlain(new this(obj), options);
    }
  }

  static createInstance(obj: Payload): ExcelValidationErrorModel;
  static createInstance(obj: Payload[]): ExcelValidationErrorModel[];
  static createInstance(obj: MaybeArray<Payload>) {
    if (Array.isArray(obj)) {
      return obj.map((item) => new this(item));
    }
    return new this(obj);
  }
}
