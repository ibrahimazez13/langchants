import sizeOf from 'image-size';
import sharp from 'sharp';

import { FileTypeResult } from '../../../modules/media/utils/file-type';
import { offloadGenerateGcd } from '../../../modules/media/utils/offload-generate-gcd';
import { rgbToHex } from '../../../modules/media/utils/rgb-to-hex';
import {
  AllowedMimeTypes,
  ValidateFileTypeResult,
} from '../../../modules/media/utils/types';

export async function extractImageMetadata(
  file: FileTypeResult & { path: string },
): Promise<ValidateFileTypeResult> {
  const [sharpMetaData, stats] = await Promise.all([
    sharp(file.path).metadata(),
    sharp(file.path).stats(),
  ]);
  let height = sharpMetaData.height;
  let width = sharpMetaData.width;
  const codec = sharpMetaData.compression;
  const space = sharpMetaData.space;

  if (height === undefined || width === undefined) {
    const dimensions = sizeOf(file.path);
    height = height ?? dimensions.height;
    width = width ?? dimensions.width;
  }

  const gcd = await offloadGenerateGcd({ width: width!, height: height! });

  const aspectRatio = `${width! / gcd}/${height! / gcd}`;

  return {
    height: height,
    width: width,
    mimeType: file.mime as AllowedMimeTypes,
    dominantColor: rgbToHex(stats.dominant),
    type: 'IMAGE',
    codecName: codec,
    space: space,
    duration: undefined,
    aspectRatio: aspectRatio,
    gcd: gcd,
  };
}
