import * as fileType from './file-type';

type FileType = typeof fileType;

export async function getFileType(): Promise<FileType> {
  return await (eval(`import('file-type')`) as Promise<FileType>);
}
