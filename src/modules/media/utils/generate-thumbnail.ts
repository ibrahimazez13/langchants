import ffmpeg from 'fluent-ffmpeg';

import { generateFileName } from '../../../modules/media/utils/generate-file-name';

export type GenerateThumbnailArgs = {
  source: string;
  destination: string;
  width: number | undefined;
  height: number | undefined;
};
export function generateThumbnail(obj: GenerateThumbnailArgs): string {
  const { source, width, height, destination } = obj;

  const filename = generateFileName('jpeg');
  let widthHeight: string | undefined;

  if (width && height) {
    widthHeight = `${width}x${height}`;
  }

  ffmpeg({
    source: source,
  }).screenshots(
    {
      timestamps: ['50%'],
      filename: filename,
      count: 1,
      size: widthHeight ? widthHeight : undefined,
    },
    destination,
  );

  return filename;
}
