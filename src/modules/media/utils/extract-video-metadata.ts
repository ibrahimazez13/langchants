import ffmpeg from 'fluent-ffmpeg';
import { join } from 'path';

import { FileTypeResult } from '../../../modules/media/utils/file-type';
import { offloadGenerateGcd } from '../../../modules/media/utils/offload-generate-gcd';
import {
  AllowedMimeTypes,
  ValidateFileTypeResult,
} from '../../../modules/media/utils/types';

type Codec = {
  codecName: string | undefined;
  width: number | undefined;
  type: string | undefined;
  height: number | undefined;
  aspectRatio: string | undefined;
};
export async function extractVideoMetadata(
  file: FileTypeResult & { path: string },
): Promise<ValidateFileTypeResult> {
  return new Promise<ValidateFileTypeResult>((resolve, reject) => {
    ffmpeg.ffprobe(join(file.path), async (error, metadata) => {
      if (error) {
        reject(error);
      }
      const { format, streams } = metadata;

      let videoCodec!: Codec | undefined;

      let audioCodec!: Codec | undefined;

      streams.forEach(function (stream) {
        if (stream.codec_type === 'video') {
          videoCodec = {
            codecName: stream.codec_name,
            width: stream.coded_width,
            type: stream.codec_type,
            height: stream.coded_height,
            aspectRatio: stream.display_aspect_ratio,
          };
        }
        if (stream.codec_type === 'audio') {
          audioCodec = {
            codecName: stream.codec_name,
            width: stream.coded_width,
            type: stream.codec_type,
            height: stream.coded_height,
            aspectRatio: stream.display_aspect_ratio,
          };
        }
      });

      const { duration } = format;

      if (!audioCodec && !videoCodec) {
        reject('Could not extract video codec nor audio codec');
      }

      if (videoCodec) {
        const gcd = await offloadGenerateGcd({
          width: videoCodec.width!,
          height: videoCodec.height!,
        });
        const aspectRatio = `${videoCodec.width! / gcd}/${
          videoCodec.height! / gcd
        }`;

        resolve({
          ...videoCodec,
          duration: duration,
          mimeType: file.mime as AllowedMimeTypes,
          type: 'VIDEO',
          aspectRatio:
            aspectRatio ??
            `${videoCodec.width! / gcd}/${videoCodec.height! / gcd}`,
          gcd: gcd,
          space: undefined,
        });
      } else {
        resolve({
          ...audioCodec!,
          duration: duration,
          type: 'AUDIO',
          mimeType: file.mime as AllowedMimeTypes,
          aspectRatio: undefined,
          space: undefined,
          gcd: undefined,
        });
      }
    });
  });
}
