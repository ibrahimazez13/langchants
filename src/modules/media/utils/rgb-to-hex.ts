function colorToHex(color: number) {
  const hex = color.toString(16);
  return hex.length === 1 ? '0' + hex : hex;
}
export function rgbToHex(rgb: { r: number; g: number; b: number }) {
  const { r, g, b } = rgb;

  return '#' + colorToHex(r) + colorToHex(g) + colorToHex(b);
}
