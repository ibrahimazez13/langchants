import sharp, { ColourspaceEnum } from 'sharp';
import { ZodIssue } from 'zod';

import { MediaType } from '@prisma/client';

import {
  AllowedMimeAudioList,
  AllowedMimeDocsList,
  AllowedMimeImageList,
  AllowedMimeVideoList,
} from '../../media/utils/constants';
import { MimeType } from '../../media/utils/file-type';

export type FileValidationCondition = (
  | {
      type: Extract<FileValidationTypes, 'NUMBER'>;
      subTypes: Array<
        | {
            type: Extract<FileValidationTypes, 'MIN' | 'MAX'>;
            limit: number;
          }
        | { type: Extract<FileValidationTypes, 'NULLABLE'> }
      >;
    }
  | {
      type: Extract<FileValidationTypes, 'ENUM'>;
      values: [string, ...string[]];
      subTypes: Array<{ type: Extract<FileValidationTypes, 'NULLABLE'> }>;
    }
  | {
      type: Extract<FileValidationTypes, 'STRING'>;
      subTypes: Array<
        | {
            type: Extract<FileValidationTypes, 'MAX_LENGTH'>;
            maxLength: `+${number}`;
          }
        | {
            type: Extract<FileValidationTypes, 'UUID'>;
          }
        | {
            type: Extract<FileValidationTypes, 'MONGO_ID'>;
          }
        | {
            type: Extract<FileValidationTypes, 'MIN_LENGTH'>;
            minLength: `+${number}`;
          }
        | { type: Extract<FileValidationTypes, 'NULLABLE'> }
        | {
            type: Extract<FileValidationTypes, 'DATE_TIME'>;
            options?: {
              message?: string | undefined;
              precision?: number | null;
              offset?: boolean;
            };
          }
        | { type: Extract<FileValidationTypes, 'EMAIL'> }
        | { type: Extract<FileValidationTypes, 'NUMERIC'> }
      >;
    }
  | {
      type: Extract<FileValidationTypes, 'BOOLEAN'>;
      subTypes: Array<{
        type: Extract<FileValidationTypes, 'NULLABLE'>;
      }>;
    }
) & {
  databaseQuery?: DatabaseQuery;
};

export type DatabaseQuery = (
  value: string | number | boolean,
) => Promise<Nullable<{ id: string | number }>>;

export type ExcelValidationOptions = {
  allowedNames: Array<string>;
  canBeEmpty: boolean;
  details: ExcelValidationOptionsDetails;
};
export type ExcelValidationOptionsDetails = Array<{
  name: string;
  condition: FileValidationCondition;
  allowDuplicates: boolean;
}>;
export const ExcelColumnNameValidationErrorReason = {
  DUPLICATED: 'DUPLICATED',
  EMPTY: 'EMPTY',
  NOT_ALLOWED_NAME: 'NOT_ALLOWED_NAME',
} as const;

export type ExcelColumnNameValidationErrorReason =
  (typeof ExcelColumnNameValidationErrorReason)[keyof typeof ExcelColumnNameValidationErrorReason];

export type ExcelColumnNameValidationError = {
  name: Nullable<string>;
  allowedNames: Array<string>;
  reason: ExcelColumnNameValidationErrorReason;
};

export type ExcelColumnValueValidationError = {
  name: string;
  issues: Array<ValidateConditionResultError | never>;
  value: Nullable<number | string | boolean>;
};

export type ExcelValidationError = {
  columnNames: Array<ExcelColumnNameValidationError>;
  columnValues: Array<ExcelColumnValueValidationError>;
};

export type ValidateExcelSheetResult<T> =
  | { isValid: true; data: Array<T>; errors: null }
  | {
      isValid: false;
      errors: ExcelValidationError;
      message: null;
    }
  | {
      isValid: false;
      errors: Array<never>;
      message: string;
    };

export type ValidateConditionResultError = Pick<ZodIssue, 'code' | 'message'>;

export type ValidateConditionResult = {
  isValid: boolean;
  parsedValue: Nullable<number | string | boolean>;
  errors: Array<ValidateConditionResultError | never>;
};

export type MediaStorage = 'PUBLIC' | 'ASSETS';

export type AllowedMimeTypes =
  | ArrayElement<typeof AllowedMimeImageList>
  | ArrayElement<typeof AllowedMimeDocsList>
  | ArrayElement<typeof AllowedMimeVideoList>
  | ArrayElement<typeof AllowedMimeAudioList>;

export const AllowedExt = {
  png: 'png',
  jpeg: 'jpeg',
  jpg: 'jpg',
  xlsx: 'xlsx',
  mov: 'mov',
  mp4: 'mp4',
  mp3: 'mp3',
  mkv: 'mkv',
  csv: 'csv',
} as const;

export type AllowedExt = (typeof AllowedExt)[keyof typeof AllowedExt];

export const AllowedExtList = Object.values(AllowedExt);

export type SystemExt = 'svg' | 'js' | 'html';

export type ExtMetadataResult = {
  mimeType: AllowedMediaTypes | SystemMimeTypes;
  path: string | null;
  ext: AllowedExt;
  allowedSize: number;
  thumbnailPath: string | undefined;
};

export const FileValidationTypes = {
  NUMBER: 'NUMBER',
  STRING: 'STRING',
  BOOLEAN: 'BOOLEAN',
  NUMBER_STRING: 'NUMBER_STRING',
  MIN: 'MIN',
  MAX: 'MAX',
  UUID: 'UUID',
  MONGO_ID: 'MONGO_ID',
  MIN_LENGTH: 'MIN_LENGTH',
  MAX_LENGTH: 'MAX_LENGTH',
  NULLABLE: 'NULLABLE',
  DATE_TIME: 'DATE_TIME',
  ENUM: 'ENUM',
  EMAIL: 'EMAIL',
  DATABASE: 'DATABASE',
  NUMERIC: 'NUMERIC',
} as const;

export type FileValidationTypes =
  (typeof FileValidationTypes)[keyof typeof FileValidationTypes];

export const SystemMimeTypes = {
  'image/svg+xml': 'image/svg+xml',
  'image/gif': 'image/gif',
  'application/javascript': 'application/javascript',
  'text/html': 'text/html',
} satisfies Pick<
  Record<MimeType, MimeType>,
  'image/gif' | 'image/svg+xml' | 'application/javascript' | 'text/html'
>;

export type SystemMimeTypes =
  (typeof SystemMimeTypes)[keyof typeof SystemMimeTypes];

export const AllowedMediaTypes: Record<AllowedMimeTypes, AllowedMimeTypes> = {
  'image/png': 'image/png',
  'image/jpeg': 'image/jpeg',
  'video/mp4': 'video/mp4',
  'video/quicktime': 'video/quicktime',
  'audio/mpeg': 'audio/mpeg',
  'video/x-matroska': 'video/x-matroska',
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  'text/csv': 'text/csv',
};

export type AllowedMediaTypes =
  (typeof AllowedMediaTypes)[keyof typeof AllowedMediaTypes];

export type CodecName = 'av1' | 'hevc' | 'h264' | 'aac' | 'h265' | string;

export type ValidateFileTypeResult = Pick<
  sharp.Metadata,
  'height' | 'width'
> & {
  dominantColor?: string;
} & {
  mimeType: AllowedMimeTypes;
  type: MediaType;
  duration: number | undefined;
  aspectRatio: string | undefined;
  codecName: CodecName | undefined;
  space: keyof ColourspaceEnum | undefined;
  gcd: number | undefined;
};

export type UploadMediaArgs = Pick<sharp.Metadata, 'width' | 'height'> & {
  url: string;
  name: string;
  mime: AllowedMimeTypes;
  ext: AllowedExt;
  path: string;
  size: number;
  dominantColor: string | undefined;
  type: MediaType;
  duration: number | undefined;
  thumbnailUrl: string | undefined;
  codecName: CodecName | undefined;
  aspectRatio: string | undefined;
  space: keyof ColourspaceEnum | undefined;
  gcd: number | undefined;
};
