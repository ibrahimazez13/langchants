import { createWriteStream, readFile } from 'fs';
import { Stream } from 'stream';

export async function storeFile(obj: { source: string; destination: string }) {
  const { source, destination } = obj;
  const imgStream = new Stream.PassThrough();

  const file = await new Promise<Buffer>((resolve, reject) =>
    readFile(source, (error, data) => {
      if (error !== null) {
        reject(error);
      }
      resolve(data);
    }),
  );

  imgStream.end(file);

  const wStream = createWriteStream(destination);

  imgStream.once('error', (err) => {
    // eslint-disable-next-line no-console
    console.error(err);
  });
  imgStream.pipe(wStream);
}
