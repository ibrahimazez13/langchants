// Recursive function to return gcd of a and b

// workerThread.js
import { parentPort } from 'worker_threads';

function generateGcd(width: number, height: number): number {
  return height === 0 ? width : generateGcd(height, width % height);
}

parentPort!.on('message', (data: { width: number; height: number }) => {
  parentPort!.postMessage(generateGcd(data.width, data.height), [
    new ArrayBuffer(8),
    new ArrayBuffer(8),
  ]);
});
