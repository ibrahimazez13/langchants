import { AllowedExt, SystemExt } from '../../../modules/media/utils/types';

export const generateFileName = (ext: AllowedExt | SystemExt | string) => {
  const randomName = Array(8)
    .fill(null)
    .map(() => Math.round(Math.random() * 16).toString(16))
    .join('');

  return `xp-${Date.now()}-${randomName}.${ext}`;
};
