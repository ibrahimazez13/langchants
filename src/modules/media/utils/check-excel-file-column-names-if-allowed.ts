import { FORBIDDEN_EXCEL_COLUMN_NAMES } from './constants';
import { ExcelColumnNameValidationError } from './types';

export function checkExcelColumnNamesIfAllowed(obj: {
  dataColumnNames: Array<string>;
  allowedNames: Array<string>;
}): Array<ExcelColumnNameValidationError> {
  const { allowedNames, dataColumnNames } = obj;

  const validationErrors: Array<ExcelColumnNameValidationError> = [];
  const duplicatedColumnNames: Array<string> = [];

  /**
   * Duplicated Items need to be checked before anything else
   * to avoid redundant results.
   * e.x if the column is duplicated and data is to be validated
   * all to together that means duplication validation is not separated
   * like this, we are going to check the column twice b.c we are running
   * into it twice, once in the inner array and the other one in the outer array.
   * that means once it will be duplicated name and the other one will be a not allowed name
   */
  dataColumnNames.forEach((name) => {
    const duplicatedColumnName = dataColumnNames.find(
      (wordToBeComparedWith) => {
        return isDuplicatedColumnName(name, wordToBeComparedWith);
      },
    );

    if (duplicatedColumnName) {
      validationErrors.push({
        name: name,
        allowedNames: allowedNames,
        reason: 'DUPLICATED',
      });
      duplicatedColumnNames.push(duplicatedColumnName);
      return;
    }
  });

  /**
   * Remove duplicated items which has already been checked
   */
  duplicatedColumnNames.map((item) => {
    const index = dataColumnNames.indexOf(item);
    if (index > -1) {
      // only splice array when item is found
      dataColumnNames.splice(index, 1); // 2nd parameter means remove one item only
    }
  });

  dataColumnNames.forEach((name) => {
    let hasEmptyColumnName = false;

    FORBIDDEN_EXCEL_COLUMN_NAMES.forEach((forbiddenColumnName) => {
      hasEmptyColumnName = name.includes(forbiddenColumnName);
    });

    if (hasEmptyColumnName) {
      validationErrors.push({
        name: null,
        allowedNames: allowedNames,
        reason: 'EMPTY',
      });
      return;
    }

    const hasAllowedName = allowedNames.includes(name);
    if (!hasAllowedName) {
      validationErrors.push({
        name: name,
        allowedNames: allowedNames,
        reason: 'NOT_ALLOWED_NAME',
      });
      return;
    }
    return;
  });

  return validationErrors;
}

const isDuplicatedColumnName = (word: string, wordToBeComparedWith: string) => {
  return new RegExp(`${word}_{1}[0-9]+`, 'g').test(wordToBeComparedWith);
};
