import { join } from 'path';
import { Worker } from 'worker_threads';

export async function offloadGenerateGcd(data: {
  width: number;
  height: number;
}): Promise<number> {
  return new Promise<number>((resolve, reject) => {
    const worker = new Worker(join(__dirname, 'generate-gcd'), {
      workerData: data,
    });

    worker.on('message', resolve);
    worker.on('error', reject);
    worker.on('exit', (code) => {
      if (code !== 0) {
        reject(new Error(`Worker stopped with exit code ${code}`));
      }
    });

    // Send data to the worker thread
    worker.postMessage(data);
  });
}
