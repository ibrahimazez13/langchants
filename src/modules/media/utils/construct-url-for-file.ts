import { MEDIAS_API_PATH } from '../../../common/utils/constants';
import { envConfig } from '../../../common/utils/env-config';
import { MediaStorageType } from '../../../modules/media/utils/constants';

export const constructUrlForFile = (obj: {
  protocol: string;
  host: string;
  fileName: string;
  endpoint: MediaStorageType;
}) => {
  const { fileName, host, protocol, endpoint } = obj;
  const origin = `${protocol}://${host}`;
  const url = new URL(origin);
  url.port = String(envConfig.PORT);
  url.pathname = `${MEDIAS_API_PATH}/${endpoint}/${fileName}`;
  return url.toString();
};
