import { join } from 'path';

import { envConfig } from '../../../common/utils/env-config';
import {
  allowedExtToMetadata,
  ExtToMetadataValue,
} from '../../../modules/media/utils/ext-to-metadata';
import {
  AllowedExt,
  AllowedMediaTypes,
  ExtMetadataResult,
  MediaStorage,
  SystemMimeTypes,
} from '../../../modules/media/utils/types';

export const extractStorageMetadata = (
  ext: AllowedExt | string,
  storage: MediaStorage,
): ExtMetadataResult | null => {
  const metaData:
    | ExtToMetadataValue<AllowedMediaTypes | SystemMimeTypes>
    | undefined = allowedExtToMetadata[ext];

  if (!metaData) {
    return null;
  }

  let path: string | null = null;
  let thumbnailPath: string | undefined;

  if (storage === 'PUBLIC') {
    if (metaData.folder.public !== null) {
      path = join(process.cwd(), envConfig.MEDIA_PATH, metaData.folder.public);
    }
  } else {
    if (metaData.folder.assets !== null) {
      path = join(process.cwd(), 'src', 'assets', metaData.folder.assets);
    }
  }

  if (metaData.folder.thumbnail) {
    if (storage === 'PUBLIC') {
      thumbnailPath = join(
        process.cwd(),
        envConfig.MEDIA_PATH,
        metaData.folder.thumbnail,
      );
    } else {
      thumbnailPath = join(
        process.cwd(),
        'src',
        'assets',
        metaData.folder.thumbnail,
      );
    }
  }

  return {
    mimeType: metaData.mime,
    path: path,
    ext: ext as AllowedExt,
    allowedSize: metaData.allowedSize,
    thumbnailPath: thumbnailPath,
  };
};
