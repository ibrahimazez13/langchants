import { extname } from 'path';

export const myExtname = (filename: string) => {
  return extname(filename).replace('.', '');
};
