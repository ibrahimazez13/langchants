import { envConfig } from '../../../common/utils/env-config';
import {
  AllowedExt,
  AllowedMediaTypes,
  SystemExt,
  SystemMimeTypes,
} from '../../../modules/media/utils/types';

type ExtToMetadataKey = AllowedExt | SystemExt | string;
export type ExtToMetadataValue<T extends AllowedMediaTypes | SystemMimeTypes> =
  {
    mime: T;
    folder: {
      public: string | null;
      assets: string | null;
      thumbnail: string | null;
    };
    allowedSize: number;
  };
export const systemExtToMetadata: Record<
  ExtToMetadataKey,
  ExtToMetadataValue<SystemMimeTypes>
> = {
  svg: {
    mime: SystemMimeTypes['image/svg+xml'],
    folder: {
      public: envConfig.IMAGE_FOLDER,
      assets: envConfig.IMAGE_FOLDER,
      thumbnail: null,
    },
    allowedSize: 1,
  },
  html: {
    mime: SystemMimeTypes['text/html'],
    folder: {
      public: envConfig.IMAGE_FOLDER,
      assets: envConfig.DOCUMENTS_FOLDER,
      thumbnail: null,
    },
    allowedSize: 1,
  },
  js: {
    mime: SystemMimeTypes['application/javascript'],
    folder: {
      public: envConfig.IMAGE_FOLDER,
      assets: envConfig.DOCUMENTS_FOLDER,
      thumbnail: null,
    },
    allowedSize: 0.08,
  },
};
export const allowedExtToMetadata: Record<
  ExtToMetadataKey,
  ExtToMetadataValue<AllowedMediaTypes | SystemMimeTypes>
> = {
  csv: {
    mime: AllowedMediaTypes['text/csv'],
    folder: {
      public: envConfig.DOCUMENTS_FOLDER,
      assets: null,
      thumbnail: null,
    },
    allowedSize: 100,
  },
  svg: {
    mime: SystemMimeTypes['image/svg+xml'],
    folder: {
      public: null,
      assets: envConfig.IMAGE_FOLDER,
      thumbnail: null,
    },
    allowedSize: 1,
  },
  jpeg: {
    mime: AllowedMediaTypes['image/jpeg'],
    folder: {
      public: envConfig.IMAGE_FOLDER,
      assets: envConfig.IMAGE_FOLDER,
      thumbnail: envConfig.THUMBNAIL_FOLDER,
    },
    allowedSize: 10,
  },
  jpg: {
    mime: AllowedMediaTypes['image/jpeg'],
    folder: {
      public: envConfig.IMAGE_FOLDER,
      assets: envConfig.IMAGE_FOLDER,
      thumbnail: envConfig.THUMBNAIL_FOLDER,
    },
    allowedSize: 10,
  },
  png: {
    mime: AllowedMediaTypes['image/png'],
    folder: {
      public: envConfig.IMAGE_FOLDER,
      assets: envConfig.IMAGE_FOLDER,
      thumbnail: null,
    },

    allowedSize: 10,
  },

  xlsx: {
    mime: AllowedMediaTypes[
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    ],
    folder: {
      public: envConfig.DOCUMENTS_FOLDER,
      assets: envConfig.DOCUMENTS_FOLDER,
      thumbnail: null,
    },

    allowedSize: 50,
  },
  mp4: {
    mime: AllowedMediaTypes['video/mp4'],
    folder: {
      public: envConfig.VIDEO_FOLDER,
      assets: envConfig.VIDEO_FOLDER,
      thumbnail: envConfig.THUMBNAIL_FOLDER,
    },
    allowedSize: 25,
  },
  mkv: {
    mime: AllowedMediaTypes['video/x-matroska'],
    folder: {
      public: envConfig.VIDEO_FOLDER,
      assets: envConfig.VIDEO_FOLDER,
      thumbnail: envConfig.THUMBNAIL_FOLDER,
    },

    allowedSize: 25,
  },
  mov: {
    mime: AllowedMediaTypes['video/quicktime'],
    folder: {
      public: envConfig.VIDEO_FOLDER,
      assets: envConfig.VIDEO_FOLDER,
      thumbnail: envConfig.THUMBNAIL_FOLDER,
    },
    allowedSize: 25,
  },
  mp3: {
    mime: AllowedMediaTypes['audio/mpeg'],
    folder: {
      public: envConfig.AUDIO_FOLDER,
      assets: envConfig.AUDIO_FOLDER,
      thumbnail: null,
    },
    allowedSize: 3,
  },
};
