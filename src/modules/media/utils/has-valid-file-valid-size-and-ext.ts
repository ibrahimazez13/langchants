import { extractStorageMetadata } from '../../../modules/media/utils/extract-storage-metadata';
import {
  AllowedExtList,
  ExtMetadataResult,
} from '../../../modules/media/utils/types';

export function checkFileSizeAndExt(obj: { size: number; ext: string }):
  | { isValidFileType: false; isFileToLarge: false }
  | { isValidFileType: true; isFileToLarge: true }
  | ({
      isValidFileType: true;
      size: number;
      isFileToLarge: false;
    } & ExtMetadataResult) {
  const { ext, size } = obj;

  const extractMetadataResult = extractStorageMetadata(ext, 'PUBLIC');

  if (!extractMetadataResult) {
    return { isValidFileType: false, isFileToLarge: false };
  }
  if (extractMetadataResult.allowedSize < size / 1024 / 1024) {
    return { isValidFileType: true, isFileToLarge: true };
  }

  if (AllowedExtList.includes(extractMetadataResult.ext)) {
    return {
      isValidFileType: true,
      size: size,
      ...extractMetadataResult,
      isFileToLarge: false,
    };
  }

  return { isValidFileType: false, isFileToLarge: false };
}
