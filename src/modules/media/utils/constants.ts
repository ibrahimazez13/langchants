import ms from 'ms';

import { MimeType } from './file-type';

export const ONE_YEAR = ms('1y') / 1000;
export const AllowedMimeImageList = [
  'image/png',
  'image/jpeg',
] satisfies Array<MimeType>;

export const AllowedMimeVideoList = [
  'video/mp4',
  'video/quicktime',
  'video/x-matroska',
] satisfies Array<MimeType>;

export const AllowedMimeAudioList = ['audio/mpeg'] satisfies Array<MimeType>;

export const AllowedMimeDocsList = [
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  'text/csv',
] satisfies Array<MimeType>;

export const FORBIDDEN_EXCEL_COLUMN_NAMES = ['__EMPTY'];

export const MediaStorageType = { PUBLIC: 'public', ASSETS: 'assets' } as const;

export type MediaStorageType =
  (typeof MediaStorageType)[keyof typeof MediaStorageType];
