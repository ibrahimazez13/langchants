export function getDuplicatedItemsInArray(
  array: Array<number | string | boolean | null>,
) {
  const duplicates = array.filter((item, index) => {
    return array.indexOf(item) !== index;
  });
  return Array.from(new Set(duplicates));
}
