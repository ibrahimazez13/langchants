import fs from 'fs';
import { join } from 'path';

import { Injectable, NotFoundException } from '@nestjs/common';

import { MediaRepository } from '../../../core/modules/global/repositories/media.repository';
import { GlobalFacade } from '../../../core/modules/global/services/global.facade';
import { extractStorageMetadata } from '../../../modules/media/utils/extract-storage-metadata';
import { generateThumbnail } from '../../../modules/media/utils/generate-thumbnail';
import { myExtname } from '../../../modules/media/utils/my-extname';
import {
  AllowedExt,
  ExtMetadataResult,
  MediaStorage,
} from '../../../modules/media/utils/types';
import { ValidateFileTypeService } from '../services/validate-file-type.service';
import { constructUrlForFile } from '../utils/construct-url-for-file';
import { storeFile } from '../utils/store-file';

@Injectable()
export class MediaService {
  constructor(
    private readonly _globalFacade: GlobalFacade,
    private readonly _mediaRepository: MediaRepository,
    private readonly _validateFileTypeService: ValidateFileTypeService,
  ) {}

  async checkIfExist(obj: {
    filename: string;
    storage: MediaStorage;
  }): Promise<Omit<ExtMetadataResult, 'thumbnailPath'>> {
    const { filename, storage } = obj;
    const ext = myExtname(filename);
    const metadata = extractStorageMetadata(ext, storage);

    if (metadata === null) {
      throw new NotFoundException();
    }
    let path: Nullable<string> = null;
    const doesFileExist = await new Promise<boolean>((resolve) => {
      if (metadata.path === null) {
        resolve(false);
        return;
      }
      fs.access(join(metadata.path, filename), fs.constants.F_OK, (err) => {
        if (err !== null) {
          resolve(false);
        }
        resolve(true);
      });
    });

    if (doesFileExist === true) {
      path = metadata.path;
    } else {
      if (metadata.thumbnailPath !== undefined) {
        const doesThumbnailExist = await new Promise<boolean>((resolve) => {
          fs.access(
            join(metadata.thumbnailPath!, filename),
            fs.constants.F_OK,
            (err) => {
              if (err !== null) {
                resolve(false);
              }
              resolve(true);
            },
          );
        });

        if (doesThumbnailExist === true) {
          path = metadata.thumbnailPath;
        }
      }
    }

    if (path === null) {
      throw new NotFoundException();
    }

    return {
      ext: metadata.ext,
      path: path,
      allowedSize: metadata.allowedSize,
      mimeType: metadata.mimeType,
    };
  }

  async uploadFile(obj: {
    protocol: string;
    host: string;
    candidateFile: Express.Multer.File;
    size: number;
    path: string;
    thumbnailPath: string | undefined;
    ext: AllowedExt;
  }) {
    const { candidateFile, thumbnailPath, host, protocol, size, path, ext } =
      obj;

    const metaData = await this._validateFileTypeService.validateFile(
      candidateFile.path,
    );

    const destination = join(path, candidateFile.filename);
    let thumbnailUrl: string | undefined;

    if (metaData.type === 'VIDEO') {
      const thumbnailFilename = generateThumbnail({
        source: candidateFile.path,
        destination: thumbnailPath!,
        height: metaData.height,
        width: metaData.width,
      });

      thumbnailUrl = constructUrlForFile({
        host: host,
        protocol: protocol,
        fileName: thumbnailFilename,
        endpoint: 'public',
      });
    }

    void (await storeFile({
      source: candidateFile.path,
      destination: destination,
    }));

    const url = constructUrlForFile({
      host: host,
      protocol: protocol,
      fileName: candidateFile.filename,
      endpoint: 'public',
    });

    return await this._mediaRepository.upload({
      size: size,
      url: url,
      width: metaData.width,
      height: metaData.height,
      mime: metaData.mimeType,
      name: candidateFile.filename,
      path: destination,
      ext: ext,
      dominantColor: metaData.dominantColor,
      type: metaData.type,
      aspectRatio: metaData.aspectRatio,
      codecName: metaData.codecName,
      duration: metaData.duration,
      space: metaData.space,
      thumbnailUrl: thumbnailUrl,
      gcd: metaData.gcd,
    });
  }

  /*
   * NOTE: deletion of Media will lead to cascade in other entities
   * in our case it's userMedia so no need to create transaction for
   * both of them
   */
  async deleteUserMedia(mediaId: string) {
    await this._mediaRepository.deleteById(mediaId);
  }
}
