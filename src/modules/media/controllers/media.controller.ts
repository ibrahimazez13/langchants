import { FastifyRequest } from 'fastify';

import {
  BadRequestException,
  Controller,
  PayloadTooLargeException,
  Post,
  Req,
  UploadedFile,
  UseInterceptors,
  Version,
  VERSION_NEUTRAL,
} from '@nestjs/common';
import { ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';

import { MEDIAS_API_PATH } from '../../../common/utils/constants';
import { MyApiResponse } from '../../../core/decorators/swagger/my-api-response.decorator';
import {
  diskStorage,
  FileFastifyInterceptor,
} from '../../../core/modules/fastify-file-interceptor';
import { GlobalFacade } from '../../../core/modules/global/services/global.facade';
import { generateFileName } from '../utils/generate-file-name';
import { checkFileSizeAndExt } from '../utils/has-valid-file-valid-size-and-ext';
import { myExtname } from '../utils/my-extname';

import { MediaService } from './media.service';

@Controller({ path: '', version: VERSION_NEUTRAL })
@ApiTags('Media')
export class MediaController {
  constructor(
    private readonly _globalFacade: GlobalFacade,
    private readonly _mediasService: MediaService,
  ) {}

  @MyApiResponse({
    created: {},
    badRequest: true,
    payloadTooLarge: true,
  })
  @ApiConsumes('multipart/form-data')
  @Version('1')
  @Post(`${MEDIAS_API_PATH}`)
  @UseInterceptors(
    FileFastifyInterceptor('file', {
      limits: { files: 1 },
      storage: diskStorage({}),
      // fileFilter: imageFileFilter,
    }),
  )
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  async uploadFile(
    @Req() req: FastifyRequest,
    @UploadedFile() file: Express.Multer.File | undefined,
  ) {
    if (!file) {
      throw new BadRequestException('please send a valid file');
    }

    const ext = myExtname(file.originalname);

    const checkFileSizeAndExtResult = checkFileSizeAndExt({
      size: file.size,
      ext: ext,
    });

    if (!checkFileSizeAndExtResult.isValidFileType) {
      throw new BadRequestException('please send a valid file');
    }

    if (checkFileSizeAndExtResult.isFileToLarge) {
      throw new PayloadTooLargeException('file to large');
    }

    const {
      size,
      path,
      ext: checkFileSizeAndExtResultExt,
    } = checkFileSizeAndExtResult;

    if (path === null) {
      throw new BadRequestException('please send a valid file');
    }

    const media = await this._mediasService.uploadFile({
      protocol: req.protocol,
      host: req.hostname,
      candidateFile: { ...file, filename: generateFileName(ext) },
      size: size,
      path: path,
      ext: checkFileSizeAndExtResultExt,
      thumbnailPath: checkFileSizeAndExtResult.thumbnailPath,
    });

    return media;
  }
}
