import { applyDecorators } from '@nestjs/common';
import {
  ApiBadGatewayResponse,
  ApiBadRequestResponse,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiInternalServerErrorResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiPayloadTooLargeResponse,
  ApiResponseOptions,
  ApiUnauthorizedResponse,
  ApiUnprocessableEntityResponse,
} from '@nestjs/swagger';
import {
  ApiResponse,
  ApiResponseMetadata,
  ApiResponseSchemaHost,
} from '@nestjs/swagger/dist/decorators/api-response.decorator';

import { HttpExceptionModel } from '../../../common/models/http-exception.model';

type MyApiResponseOptions =
  | Omit<ApiResponseMetadata, 'status'>
  | ApiResponseSchemaHost;

type MyApiResponseArgs = {
  notFound?: MyApiResponseOptions | true;
  badRequest?: MyApiResponseOptions | true;
  ok?: MyApiResponseOptions | true;
  unprocessable?: MyApiResponseOptions | true;
  created?: MyApiResponseOptions | true;
  noContent?: true;
  conflicted?: MyApiResponseOptions | true;
  payloadTooLarge?: MyApiResponseOptions | true;
  unAuthorized?: MyApiResponseOptions | false;
  forbidden?: MyApiResponseOptions | false;
  internalServerError?: MyApiResponseOptions | false;
  badGateway?: MyApiResponseOptions | false;
  seeOther?: MyApiResponseOptions | true;
};
function convertBooleanFieldToApiResponseOptions(obj: {
  [Key in keyof MyApiResponseArgs]:
    | Omit<MyApiResponseArgs[Key], 'true' | 'false'>
    | boolean;
}): {
  [K in keyof MyApiResponseArgs]: Omit<
    MyApiResponseArgs[K],
    'true' | 'boolean' | 'false'
  >;
} {
  for (const [key, value] of Object.entries(obj)) {
    if (value && typeof value === 'boolean')
      obj[key as keyof MyApiResponseArgs] = {} as ApiResponseOptions;
  }
  return obj;
}

export const MyApiResponse = (_args: MyApiResponseArgs) => {
  const {
    unAuthorized,
    forbidden,
    internalServerError,
    badGateway,
    ...newArgs
  } = _args;

  const args = convertBooleanFieldToApiResponseOptions({
    ...newArgs,
    badGateway: badGateway ?? true,
    internalServerError: internalServerError ?? true,
    unAuthorized: unAuthorized ?? true,
    forbidden: forbidden ?? true,
  });
  let decorators: Array<MethodDecorator & ClassDecorator> = [];

  if (args.ok) {
    decorators = [...decorators, ApiOkResponse(args.ok)];
  }
  if (args.seeOther) {
    decorators = [
      ...decorators,
      ApiResponse({
        ...args.seeOther,
        description: 'See Other',
        status: 303,
      }),
    ];
  }

  if (args.created) {
    decorators = [...decorators, ApiCreatedResponse(args.created)];
  }
  if (args.notFound) {
    decorators = [
      ...decorators,
      ApiNotFoundResponse({ type: HttpExceptionModel, ...args.notFound }),
    ];
  }

  if (args.badRequest) {
    decorators = [
      ...decorators,
      ApiBadRequestResponse({ type: HttpExceptionModel, ...args.badRequest }),
    ];
  }

  if (args.unprocessable) {
    decorators = [
      ...decorators,
      ApiUnprocessableEntityResponse({
        type: HttpExceptionModel,
        ...args.unprocessable,
      }),
    ];
  }

  if (args.noContent) {
    decorators = [...decorators, ApiNoContentResponse(args.noContent)];
  }

  if (args.conflicted) {
    decorators = [...decorators, ApiConflictResponse(args.conflicted)];
  }

  if (args.payloadTooLarge) {
    decorators = [
      ...decorators,
      ApiPayloadTooLargeResponse(args.payloadTooLarge),
    ];
  }

  if (args.badGateway) {
    decorators = [
      ...decorators,
      ApiBadGatewayResponse({
        description: 'Bad Gateway',
        status: 502,
        content: {
          'text/html': {},
        },
      }),
    ];
  }

  if (args.internalServerError) {
    decorators = [
      ...decorators,
      ApiInternalServerErrorResponse({
        type: HttpExceptionModel,
        description: 'Internal server error',
        status: 500,
      }),
    ];
  }

  if (args.unAuthorized) {
    decorators = [
      ...decorators,
      ApiUnauthorizedResponse({
        type: HttpExceptionModel,
        description: 'Unauthorized',
        status: 401,
      }),
    ];
  }

  if (args.forbidden) {
    decorators = [
      ...decorators,
      ApiForbiddenResponse({
        type: HttpExceptionModel,
        description: 'Forbidden',
        status: 403,
      }),
    ];
  }

  return applyDecorators(...decorators);
};
