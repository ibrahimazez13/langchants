import { Observable, throwError, TimeoutError } from 'rxjs';
import { catchError, timeout } from 'rxjs/operators';

import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
  RequestTimeoutException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';

import { REQUEST_TIMEOUT } from '@common/utils/constants';
import { envConfig } from '@common/utils/env-config';

@Injectable()
export class TimeoutInterceptor implements NestInterceptor {
  constructor(private readonly _reflector: Reflector) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<unknown> {
    const requestTimeout =
      this._reflector.get<number | undefined>(
        REQUEST_TIMEOUT,
        context.getHandler(),
      ) ?? envConfig.REQUEST_TIMEOUT;

    return next.handle().pipe(
      timeout(requestTimeout),
      catchError((err) => {
        if (err instanceof TimeoutError) {
          const timeoutException = new RequestTimeoutException();

          return throwError(() => timeoutException);
        }
        return throwError(() => err);
      }),
    );
  }
}
