/* eslint-disable @typescript-eslint/no-explicit-any */
import { Options } from 'multer';

import { Type } from '@nestjs/common';
import { ModuleMetadata } from '@nestjs/common/interfaces';

export type MulterModuleOptions = Options;

export interface FastifyMulterOptionsFactory {
  createMulterOptions(): Promise<MulterModuleOptions> | MulterModuleOptions;
}

export interface FastifyMulterModuleAsyncOptions
  extends Pick<ModuleMetadata, 'imports'> {
  useExisting?: Type<FastifyMulterOptionsFactory>;
  useClass?: Type<FastifyMulterOptionsFactory>;
  useFactory?: (
    ...args: any[]
  ) => Promise<MulterModuleOptions> | MulterModuleOptions;
  inject?: any[];
}
