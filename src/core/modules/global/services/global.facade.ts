import { Injectable } from '@nestjs/common';
import { I18nService, TranslateOptions } from 'nestjs-i18n';

import { PrismaService } from '@core/modules/global/services/prisma.service';

import { LanguageCode, PathImpl2 } from '../../../../common/utils/types';
import { I18nTranslations } from '../../../../generated/i18n.generated';

@Injectable()
export class GlobalFacade {
  constructor(
    public readonly i18nService: I18nService<I18nTranslations>,
    public readonly _prismaService: PrismaService,
  ) {}

  get prismaService() {
    return this._prismaService;
  }

  translate(
    value: PathImpl2<I18nTranslations>,
    options: TranslateOptions = {},
  ): string {
    return this.i18nService
      .translate(value, {
        ...options,
        lang: LanguageCode['en-US'],
      })
      .toString();
  }
}
