import { Global, Module } from '@nestjs/common';

import { MediaRepository } from './repositories/media.repository';
import { FilterService } from './services/filter.service';
import { GlobalFacade } from './services/global.facade';
import { PrismaService } from './services/prisma.service';

@Global()
@Module({
  providers: [GlobalFacade, FilterService, MediaRepository, PrismaService],
  exports: [GlobalFacade, FilterService, MediaRepository, PrismaService],
})
export class GlobalModule {}
