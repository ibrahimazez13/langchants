import { Injectable } from '@nestjs/common';

import { UploadMediaArgs } from '../../../../modules/media/utils/types';
import { selectMediaValidator } from '../../../../modules/media/validators/select-media.validator';
import { GlobalFacade } from '../services/global.facade';

@Injectable()
export class MediaRepository {
  constructor(private readonly _globalFacade: GlobalFacade) {}

  async upload(uploadMediaArgs: UploadMediaArgs) {
    const {
      name,
      codecName,
      dominantColor,
      url,
      ext,
      path,
      height,
      size,
      width,
      mime,
      type,
      duration,
      thumbnailUrl,
      aspectRatio,
      space,
      gcd,
    } = uploadMediaArgs;

    return this._globalFacade.prismaService['media'].create({
      data: {
        url: url,
        name: name,
        size: size,
        ext: ext,
        path: path,
        width: width,
        height: height,
        mime: mime,
        dominantColor: dominantColor,
        type: type,
        duration: duration,
        thumbnailUrl: thumbnailUrl,
        codec: codecName,
        aspectRatio: aspectRatio,
        space: space,
        gcd: gcd,
      },
      select: selectMediaValidator,
    });
  }

  /*
   * NOTE: deletion of Media will lead to cascade in other entities
   * like userMedia
   * */
  async deleteById(mediaId: string) {
    await this._globalFacade.prismaService['media'].delete({
      where: { id: mediaId },
      select: null,
    });
  }
}
