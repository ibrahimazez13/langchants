# Install dependencies only when needed
FROM node:18.18.0-alpine as build


WORKDIR /home/app

COPY package.json .
COPY pnpm-lock.yaml .

RUN npm i -g pnpm@8.12.0
RUN pnpm i

COPY . .
ARG PROJECT_SHORTCUT

RUN pnpm exec prisma generate
RUN pnpm run build:${PROJECT_SHORTCUT}

FROM node:18.18.0-alpine as stage

WORKDIR /home/app

RUN npm i -g pnpm@8.12.0
RUN apk add  --no-cache ffmpeg
COPY --from=build /home/app ./
ARG PROJECT_SHORTCUT
ENV PROJECT_SHORTCUT $PROJECT_SHORTCUT
#ENV PATH /home/app/node_modules/.bin:$PATH

CMD pnpm run run:container:${PROJECT_SHORTCUT}


